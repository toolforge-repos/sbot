# sbot
Various scripts. Bots run as [SteinsplitterBot](https://commons.wikimedia.org/wiki/User:SteinsplitterBot).

## Jobs
Scheduled jobs use the [Toolforge jobs framework](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Jobs_framework). The configuration is stored in `jobs.yaml`; run `toolforge jobs load jobs.yaml` to reload them.
