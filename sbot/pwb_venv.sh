#!/bin/bash
# Install Pywikibot in a virtual environment from git sources
# (Pywikibot's PyPi package does not include scripts)
# See https://wikitech.wikimedia.org/wiki/Help:Toolforge/Running_Pywikibot_scripts_(advanced)

cd `dirname $0`

# clean up directories if they already exist
rm -fdr pwbvenv pywikibot-core

# create a new virtual environment
python3 -m venv pwbvenv

# activate it
source pwbvenv/bin/activate

# clone Pywikibot
git clone --recursive --branch stable "https://gerrit.wikimedia.org/r/pywikibot/core" pywikibot-core

# install dependencies
pip install --upgrade pip setuptools wheel
pip install ./pywikibot-core[mwoauth,mysql]
pip install toolforge
