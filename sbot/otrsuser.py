#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pywikibot
import toolforge

text = ''

with toolforge.connect('centralauth') as centralauth, toolforge.connect('commonswiki') as commons:
    with centralauth.cursor() as cursor:
        cursor.execute("""SELECT gu_name
                       FROM global_user_groups
                       INNER JOIN globaluser ON global_user_groups.gug_user=globaluser.gu_id
                       WHERE gug_group = 'vrt-permissions'
                       """)
        users = cursor.fetchall()
    with commons.cursor() as cursor:
        cursor.execute("""SELECT user_name, user_editcount, ug_group
                       FROM user
                       LEFT JOIN (SELECT ug_group, ug_user FROM user_groups WHERE ug_group = "sysop") AS ugug
                       ON user_id=ug_user
                       WHERE user_name IN %s
                       ORDER BY user_name ASC
                       """, ([gu_name for (gu_name,) in users],))
        for user_name, user_editcount, ug_group in cursor.fetchall():
            uname = user_name.decode('utf-8')
            text += '|-\n'
            text += f'| [[User:{uname}|{uname}]]\n'
            text += f'| {user_editcount}\n'
            text += f'| {"Y" if ug_group is not None else ""}\n'

site = pywikibot.Site()
page = pywikibot.Page(site, "Template:PermissionTicket/Users/raw")
page.put(text, summary=u'Bot: Updated page.')
