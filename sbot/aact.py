#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pywikibot
import toolforge

conn = toolforge.connect('commonswiki')

cursor = conn.cursor()
cursor.execute("""SELECT
 actor_name AS sysop,
 count(rc_id) AS admin_actions
FROM recentchanges
INNER JOIN actor
 ON rc_actor = actor_id
WHERE rc_log_type IN ("block","delete","import","protect","rights", "merge", "massmessage", "abusefilter")
AND rc_type != "5"
AND actor_user IN (
                   SELECT ug_user
                   FROM user_groups
                   WHERE ug_group = "sysop"
)
GROUP BY actor_name
ORDER BY admin_actions DESC
LIMIT 1000;""")

text = '{{/H|1=~~~~~}}\n'

for sysop, admin_actions in cursor.fetchall():
    text += u'# {{suser|username=%s|actions=%s}}\n' %(sysop.decode("utf-8"),admin_actions)
    
cursor.close()

cursor = conn.cursor()
cursor.execute("""SELECT user_name
FROM user_groups
INNER JOIN user
 ON user_id = ug_user
WHERE ug_group = "sysop"
AND ug_user NOT IN (
                    SELECT
                     actor_user
                    FROM recentchanges
                    INNER JOIN actor
                     ON rc_actor = actor_id
                    WHERE rc_log_type IN ("block","delete","import","protect","rights", "merge", "massmessage", "abusefilter")
                    AND rc_type != "5"
                    )
LIMIT 500;""")

for user_name, in cursor.fetchall():
    text += u'# {{suser|username=%s|actions=0}}\n' %(user_name.decode("utf-8"))

text += u'|}\n'
text += u'{{:Commons:Administrators/Lists of administrators|RightsType=Administrators}}'

site = pywikibot.Site()
page = pywikibot.Page(site, "Commons:List of administrators by recent activity")
page.put(text, summary=u'Bot: Updated page.')

cursor.close()
conn.close()
