
#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Published by Hazard-SJ (https://wikitech.wikimedia.org/wiki/User:Hazard-SJ)
# under the terms of Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
# https://creativecommons.org/licenses/by-sa/3.0/
# Improvements by Steinsplitter under the same license.

from __future__ import unicode_literals
from json import dumps
import re
import mwparserfromhell
import pywikibot
from pywikibot.data.api import Request
import sys
if sys.version_info[0] >= 3:
    unicode = str


pywikibot.config.family = "commons"
pywikibot.config.mylang = "commons"

site = pywikibot.Site()
site.login()


class RenameCategoryBot(object):
    def __init__(self):
        self.doTaskPage = pywikibot.Page(site, "User:SteinsplitterBot/cm")
        self.queuePage = pywikibot.Page(site, "User:CommonsDelinker/commands")

    def checkDoTaskPage(self):
        try:
            text = self.doTaskPage.get(force = True)
        except pywikibot.IsRedirectPage:
            raise Exception(
                "The 'do-task page' (%s) is a redirect." %
                    self.doTaskPage.title(as_link = True)
            )
        except pywikibot.NoPage:
            raise Exception(
                "The 'do-task page' (%s) does not exist." %
                    self.doTaskPage.title(as_link = True)
            )
        else:
            if text.strip().lower() == "true":
                return True
            else:
                raise Exception(
                    "The task has been disabled from the 'do-task page' (%s)." %
                        self.doTaskPage.title(as_link = True)
                )

    def copyCat(self):
        if self.newCat.exists() or not self.oldCat.exists():
            return
        authors = ", ".join(self.oldCat.contributingUsers())
        summary = "[[Commons:Bots|Bot]]: Renamed from %s; the authors were %s" % \
            (self.oldCat.title(as_link = True), authors)
        if len(summary) > 255:
            newCatTalk = self.newCat.toggleTalkPage()
            try:
                talkText = newCatTalk.get()
                talkText += "\n\n"
            except pywikibot.NoPage:
                talkText = ""
            finally:
                talkText += "== Authors ==\nThe authors of the original category page are:\n* [[User:%s]]\n~~~~" % \
                    "]]\n * [[User:".join(self.oldCat.contributingUsers())
                newCatTalk.put(
                    talkText,
                    summary = "[[Commons:Bots|Bot]]: Listifying authors of the original category page"
                )
        catText = self.oldCat.get()
        catCode = mwparserfromhell.parse(catText)
        templates = catCode.filter_templates()
        for template in templates:
            if template.name.lower().strip() == "move":
                catCode.remove(template)
        self.newCat.put(unicode(catCode).strip(), summary)

    def moveCat(self):
        if self.newCat.exists() or not self.oldCat.exists():
            return
        summary = u"Per request by [[User:" + self.by + "|" + self.by + "]] on [[COM:CDC]]: " + self.reason

        self.oldCat.move(self.newCat.title(), reason=summary)
        catText = self.newCat.get(force=True)
        catCode = mwparserfromhell.parse(catText)
        templates = catCode.filter_templates()
        for template in templates:
            if template.name.lower().strip() == "move":
                catCode.remove(template)
        self.newCat.put(unicode(catCode).strip(), "Bot: Removed {{[[Template:Move|move]]}}")

    def run(self):
        queueText = self.queuePage.get(force = True)
        queueCode = mwparserfromhell.parse(queueText)
        templates = queueCode.filter_templates()
        #templateNames = [temp.name.lower().strip() for temp in templates]
        #if "stop" in templateNames:
        #    raise Exception(
        #        "The task has been disabled from the 'queue page' (%s)." %
        #            self.queuePage.title(asLink = True)
        #    )
        for template in templates:
            self.checkDoTaskPage()
            if not template.name.lower().strip() == "move cat":
                continue
            if template.has_param(1) and template.has_param(2):
                self.oldCat = pywikibot.Category(site, template.get(1).value)
                self.newCat = pywikibot.Category(site, template.get(2).value)
            if template.has_param("reason"):
                reason = template.get("reason").value.strip()
            elif template.has_param(3):
                reason = template.get(3).value.strip()
            else:
                reason = None
            self.reason = reason
            if template.has_param("user"):
                by = template.get("user").value.strip()
            elif template.has_param(4):
                by = template.get(4).value.strip()
            self.by = by

            pages = list()
            pages.extend(list(self.oldCat.articles()))
            pages.extend(list(self.oldCat.subcategories()))
            if not self.oldCat.exists():
                continue
            if not self.newCat.exists():
                self.moveCat()
                self.oldCat = pywikibot.Category(site, template.get(1).value)
            for page in pages:
                oldText = page.get()
                newText = pywikibot.textlib.replaceCategoryInPlace(oldText, self.oldCat, self.newCat)
                if oldText != newText:
                    page.put(
                        newText,
                        summary = "Bot: Replaced category %s with %s%s" % (
                            self.oldCat.title(as_link = True),
                            self.newCat.title(as_link = True),
                            " (requested by [[User:%s|%s]]; given reason: '%s')" % (by, by, reason if reason else "")
                        )
                    )

                    self.oldCat.put(
                        "{{Category redirect|Category:%s}}" % self.newCat.title(with_ns = False),
                        summary = "[[Commons:Bots|Bot]]: Redirected renamed category to %s%s" % (
                            self.newCat.title(as_link = True),
                            " (requested by [[User:%s|%s]]; given reason: '%s')" % (by, by, reason if reason else "")
                        )
                    )

def main():
    bot = RenameCategoryBot()
    bot.run()

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()

