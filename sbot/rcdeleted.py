#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pywikibot

pywikibot.output(u'\nScanner started. Connecting with database...')

import toolforge
conn = toolforge.connect('commonswiki')

pywikibot.output(u'\nConnected with database. Executing query...')

cursor = conn.cursor()
cursor.execute("""SELECT
 DATE_FORMAT(img_timestamp, "%b %d %Y %h:%i %p") AS ts,
 REPLACE( img_name ,'_',' ' ) AS img,
 actor_name AS user,
 user_editcount AS uec,
 REPLACE( fa_name ,'_',' ' ) AS dimg,
 second_actor_name AS duser
FROM image
INNER JOIN actor
 ON img_actor = actor_id
INNER JOIN (SELECT
             fa_sha1,
             fa_name,
             actor_name AS second_actor_name
            FROM filearchive
            INNER JOIN actor
             ON fa_actor = actor_id
            ) AS deletedfile
ON fa_sha1 = img_sha1
INNER JOIN user ON
 actor_user = user_id
WHERE img_timestamp > DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -60 DAY), '%Y%m%d%H%i%s')
AND user_editcount < 35
ORDER BY img_timestamp DESC;""")

text = 'Uploads of previously deleted files in the last 35 days by users with low editcount. Data as of ~~~~~.\n\n'
text += u'{| class="wikitable sortable" style="text-align:center"\n'
text += u'|-\n'
text += u'! class="sortable" | Timestamp !! File !! Uploader !! Deleted file !! Uploader\n'

for ts, img, user, uec, dimg, duser in cursor.fetchall():
    text += u'{{/loader|ts=%s' %(ts)
    text += u'|img=%s' %(img.decode("utf-8"))
    text += u'|user=%s|edits=%s' %(user.decode("utf-8"),uec)
    text += u'|file2=%s' %(dimg.decode("utf-8"))
    if not duser:
        duser = '?unknown?'
    text += u'|user2=%s}}\n' %(duser.decode("utf-8"))

cursor.close()

text += u'|}\n'

pywikibot.output(u'\nCreating report...\n')

site = pywikibot.Site()
page = pywikibot.Page(site, "User:SteinsplitterBot/Previously deleted files")
page.put(text, summary=u'Update', watchArticle=None, minorEdit=False, botflag=False)

cursor.close()
conn.close()
