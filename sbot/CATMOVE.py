#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pywikibot
import toolforge

pywikibot.output(u'\nConnecting with database...')

conn = toolforge.connect('commonswiki')

pywikibot.output(u'\nConnected with database. Starting query...')

cursor = conn.cursor()
cursor.execute("""SELECT
  actor_name AS username,
  COUNT(log_id) AS count
FROM logging_logindex
INNER JOIN actor
 ON log_actor = actor_id
WHERE log_type = "move"
AND log_namespace = 6
GROUP BY actor_name
ORDER BY COUNT(actor_name) DESC
Limit 300;""")

text = '{{Commons:File renaming/Stats/Des|1=~~~~~}}\n'
text += u'{| class="wikitable sortable" style="text-align:center"\n'
text += u'|-\n'
text += u'! class="sortable" | User !! Count\n'

for username, count in cursor.fetchall():
    text += u'|-\n'
    text += u'| [[User:%s|%s]]\n' %(username.decode("utf-8"),username.decode("utf-8"))
    text += u'| %s\n' %(count)

text += u'|}'

pywikibot.output(u'\nQUERY OK. Generating report...\n')

site = pywikibot.Site()
page = pywikibot.Page(site, "Commons:File renaming/Stats")
page.put(text, summary=u'Bot: Updated page.')

cursor.close()
conn.close()
