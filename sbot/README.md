# SteinsplitterBot Pywikibot scripts
## Setup
```
$ edit ~/.pywikibot/user-config.py # Add bot configuration, in particular the OAuth access tokens
$ chmod 600 ~/.pywikibot/user-config.py # The access tokens are sensitive, don’t let others read them
$ toolforge jobs run setup-venv --command $HOME/sbot/pwb_venv.sh --image python3.11 --wait
```

## Configuration
Scheduled jobs are stored in `jobs.yaml` in the parent directory. See the README there for details.

## Testing
Run `SCRIPTNAME.py` once:
```
$ toolforge jobs run SCRIPTNAME --image python3.11 --command "$HOME/sbot/pwbvenv/bin/python3 $HOME/sbot/SCRIPTNAME.py"
```
