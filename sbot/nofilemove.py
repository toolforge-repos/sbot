#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pywikibot

import toolforge
conn = toolforge.connect('commonswiki')

cursor = conn.cursor()
cursor.execute("""SELECT
 DATE_FORMAT(rc_timestamp, "%b %d %Y %h:%i %p") AS timestamp,
 REPLACE(ug_group,'sysop','admin') AS usergroup,
 actor_name AS user,
 REPLACE( CONCAT( 'File:', rc_title),'_',' ' ) AS file,
 comment_text AS comment,
 rc_this_oldid AS diff
FROM recentchanges
INNER JOIN actor
 ON rc_actor = actor_id
INNER JOIN
  (
   SELECT
   ug_user,
   ug_group
   FROM user_groups
   WHERE ug_group = "sysop"
   OR ug_group = "filemover"
   ) AS ugu
ON actor_user = ug_user
INNER JOIN comment
 ON rc_comment_id = comment_id
WHERE rc_namespace = "6"
AND rc_source = "mw.edit"
AND (ug_group="sysop" OR ug_group="filemover")
AND rc_timestamp > DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -3 DAY), '%Y%m%d%H%i%s')
AND (
      comment_text LIKE "%No valid reason stated, see the [[COM:MOVE|rename guidelines]]%"
      OR comment_text LIKE "%No valid reason stated, see the rename guidelines%"
      OR comment_text LIKE "%see the [[COM:MOVE|rename guidelines]]%"
      OR comment_text LIKE "%see the [[COM:RENAME|rename guidelines]]%"
      OR comment_text LIKE "%see the [[COM:FR|rename guidelines]]%"
      OR comment_text LIKE "%rename request declined%"
      OR comment_text LIKE "%rename request declined%"
     )
ORDER BY rc_timestamp DESC
LIMIT 120;""" )

rc = cursor.rowcount

text = '{{/H|count=%s}}\n' %(rc)

for timestamp, usergroup, user, file, comment, diff in cursor.fetchall():
    text += u'{{Autotranslate|1=%s|2=%s|3=%s|4=%s|5=%s|6=<nowiki>%s</nowiki>|base=RDRR}}\n' %(file.decode("utf-8"),diff,timestamp,usergroup.decode("utf-8"),user.decode("utf-8"),comment.decode("utf-8"))

site = pywikibot.Site()
page = pywikibot.Page(site, "Commons:File renaming/Recently declined rename requests")

text_old = page.get()
if text.strip() != text_old.strip():
    page.put(text, summary=u'Bot: Updated page.')
else:
    print("The report is up to date. No change was neccesary")

cursor.close()
conn.close()
